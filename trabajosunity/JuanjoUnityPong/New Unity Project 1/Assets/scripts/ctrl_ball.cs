﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ctrl_ball : MonoBehaviour {

	private Rigidbody2D rb;
	public ctrl_score gm;
	// Use this for initialization
	void Start () {
		//StartCoroutine(Time());
		rb = GetComponent<Rigidbody2D> ();
		rb.velocity = new Vector2 (6f, 6f);
	}
	
	// Update is called once per frame
	void Update () {
		if (this.transform.position.x >= 16f) {
			this.transform.position = new Vector3 (0f, 0f, 0f);
			rb.velocity = Vector2.zero;
			rb.velocity = new Vector2 (6f, 6f);
		}
		if (this.transform.position.x <= -16f) {
			this.transform.position = new Vector3 (0f, 0f, 0f);
			rb.velocity = Vector2.zero;
			rb.velocity = new Vector2 (6f, 6f);
		}

	}
	void OnTriggerEnter2D (Collider2D other)
	{
		print (other.transform.name);
		if (other.transform.name == "Player_2") {
			gm.UpdateScore ("P1");

		}
		if (other.transform.name == "Player_1") {
			gm.UpdateScore ("P2");

		}
	}
}
