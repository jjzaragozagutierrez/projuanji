﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ctrl_score : MonoBehaviour {
	public Text board ;
	public GameObject ball;

	private int score_1 = 0;
	private int score_2 = 0;

	// Use this for initialization
	void Start () {
		ball = GameObject.Find ("ball");
	}
	

	public void UpdateScore (string player) {

		if (player == "P1") {
			score_1++ ;
		}

		if (player == "P2") {
			score_2++ ;
		}

		board.text = score_1.ToString () + "-" + score_2.ToString ();
		print (score_1 + " - " + score_2);

	}
}
