﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

	[SerializeField]GameObject prefabToSpawn;


	// Use this for initialization
	void Start () {
		for (int i = 0; i < 10; i++) {
			float rndX = Random.Range (-10f, 10f);
			float rndY = Random.Range (-10f, 10f);
			Instantiate (prefabToSpawn, new Vector3(rndX,rndY,0), Quaternion.identity);	
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
