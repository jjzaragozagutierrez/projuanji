﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
public class ctrl : MonoBehaviour {
	public Text cont;
	public float speed = 1;             //Floating point variable to store the player's movement speed.

	//private Rigidbody2D rb2d;       //Store a reference to the Rigidbody2D component required to use 2D Physics.

	// Use this for initialization
	void Start()
	{
		//Get and store a reference to the Rigidbody2D component so that we can access it.
		//rb2d = GetComponent<Rigidbody2D> ();
	}

	//FixedUpdate is called at a fixed interval and is independent of frame rate. Put physics code here.
	void FixedUpdate()
	{
		if (Input.GetKey (KeyCode.W)) {
			transform.Translate (Vector2.up * Time.deltaTime * speed);
		}
		if (Input.GetKey (KeyCode.S)) {
			transform.Translate (Vector2.down * Time.deltaTime * speed);
		}
		if (Input.GetKey (KeyCode.A)) {
			transform.Translate (Vector2.left * Time.deltaTime * speed);
		}
		if (Input.GetKey (KeyCode.D)) {
			transform.Translate (Vector2.right * Time.deltaTime * speed);

			//Call the AddForce function of our Rigidbody2D rb2d supplying movement multiplied by speed to move our player.

		}
	}
	void OnTriggerEnter2D (Collider2D other) {
		if (other.gameObject.CompareTag("monky")) {
			cont.text = (Convert.ToInt32 (cont.text) + 1).ToString ();
			Destroy (other.gameObject);


		}
	
	}

}