﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

//	[SerializeField]GameObject prefabToSpawn;
	private Transform enemyHolder;
	public float speed;
	public Transform shotSpawn;

	public float nextFire;
	public GameObject shot;
	public float fireRate;
	// Use this for initialization
	void Start () {
		InvokeRepeating ("MoveEnemy", 0.1f, 0.3f);
		enemyHolder = GetComponent<Transform> ();
	}
	
	// Update is called once per frame
	void MoveEnemy () {
		enemyHolder.position += Vector3.right * speed;
		foreach (Transform enemy in enemyHolder) {
			if (enemy.position.x < -10.5 || enemy.position.x > 10.5) {
				speed = -speed;
				enemyHolder.position += Vector3.down * 0.5f;
				return;
			}
			if (Random.value > fireRate) {
				Instantiate (shot, enemy.position, enemy.rotation);
			
			}
		}
	}


}
