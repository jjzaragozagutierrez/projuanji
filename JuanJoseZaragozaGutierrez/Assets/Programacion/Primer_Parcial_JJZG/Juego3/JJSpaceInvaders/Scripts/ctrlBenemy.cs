﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ctrlBenemy : MonoBehaviour {
	private Transform bullet;
	public float speed;

	// Use this for initialization
	void Start () {
		bullet = GetComponent<Transform> ();

	}
	
	// Update is called once per frame
	void FixedUpdate () {
		bullet.position += Vector3.up * -speed;
		if (bullet.position.y <= -10)
			Destroy (bullet.gameObject);
	}
	void OnTriggerEnter2D(Collider2D other){
		
		if (other.tag == "Player") {
			Destroy (other.gameObject);
			Destroy (gameObject);
		} else if (other.tag == "Base") {
			GameObject playerBase = other.gameObject;
			Basevida basevida = playerBase.GetComponent<Basevida> ();
			basevida.healt -= 1;
			Destroy (gameObject);
		
		}
		if (other.tag == "Bullet") {
			Destroy (other.gameObject);
			Destroy (gameObject);
		}
	}
}
