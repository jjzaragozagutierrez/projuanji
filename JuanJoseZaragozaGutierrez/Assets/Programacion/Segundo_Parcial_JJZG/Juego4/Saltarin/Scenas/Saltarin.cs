﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Saltarin : MonoBehaviour {
	[SerializeField] float accumulatedForce = 0;
	private bool jump;
	public Rigidbody rb;
	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void Update () {
 		if (Input.GetKey (KeyCode.Space) && !jump)
			jump = true;
			accumulatedForce += 10;
		rb.AddForce (new Vector3 (0, 10, 0));
		
	}



}
